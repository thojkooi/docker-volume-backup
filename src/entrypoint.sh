#!/bin/bash
set -e

echo "Starting back-up entry point.."
if [ "$1" = 'backup' ]; then
    /bin/bash /scripts/backup.sh
    exit 0
fi

exec "$@"

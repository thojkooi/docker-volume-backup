# Docker volume back-up

[![build status](https://gitlab.com/thojkooi/docker-volume-backup/badges/master/build.svg)](https://gitlab.com/thojkooi/docker-volume-backup/commits/master)

Docker helper image for creating `tar.gz` files of a docker-volume using daily and weekly back-ups. Includes back-up rotation.

# Configuration

## Environment variables

| Variable | Description | Default value |
|----------|-------------|---------------|
| DAY_OF_WEEK_TO_KEEP | The day on which the weekly back-up will run (1-7 = Monday-Sunday) | `5` |
| DAYS_TO_KEEP | The amount of daily backups to keep | `2` |
| WEEKS_TO_KEEP | The amount of weekly backups to keep | `3` |

## Volumes

| Volume | Description |
|--------|-------------|
|`/docker-volume-data`|Directory that will be backed-up when this container runs. Requires read access only|
|`/backups`|Directory where the backups will be placed. Requires write access|

# Examples

### File system directories

```bash
docker run --rm \
    -v /data/postgres:/docker-volume-data:ro \
    -v /data/backups/postgres:/backups \
    registry.gitlab.com/thojkooi/docker-volume-backup
```

### Named volumes

```bash
docker run --rm \
    -v named_volume:/docker-volume-data:ro \
    -v /data/backups/postgres:/backups \
    registry.gitlab.com/thojkooi/docker-volume-backup
```
